#!/bin/make

all: kinetic_formalism.pdf kinetic_formalism_competition.pdf

R=R

%.pdf: %.svg
	inkscape -A $@ $<
	pdfcrop $@
	mv $(dir $@)*-crop.pdf $@

%.png: %.svg
	inkscape -e $@ -d 300 $<

%.pdf: %.png
	bmeps -lpdf -olevel=1.4 $< $@

%.tex: %.Rnw
	$(R) --encoding=utf-8 -e "library('knitr'); knit('$<')"

%.pdf: %.tex $(wildcard *.bib) $(wildcard *.tex)
	latexmk -f -pdf -pdflatex='xelatex -interaction=nonstopmode %O %S' -bibtex -use-make $<

clean:
	latexmk -C
	rm -f kinetic_formalism.tex kinetic_formalism_competition.tex
	rm -rf cache
	rm -f kinetic_formalism_competition.pdf kinetic_formalism.pdf

.PHONY: clean all

.DELETE_ON_ERROR: %.tex
